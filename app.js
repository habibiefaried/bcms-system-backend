var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var expressSanitizer = require('express-sanitizer');

var app = express();

app.path = require('path');
app.mongoose = require('mongoose');
require('mongoose-long')(app.mongoose); //kalau memang pakai long
app.mongoose.connect('mongodb://localhost:27017/bcms');

app.set('port', process.env.PORT || 12345);
app.use(bodyParser.json());
app.use(expressSanitizer());

/** allow cross domain request **/
app.use(function(req,res,next){
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'HEAD,GET,POST,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With, Origin, Accept, X-HTTP-Method-Override');

	// intercept OPTIONS method
	if (req.method == 'OPTIONS') {
	  res.sendStatus(200);
	} else {
	  next();
	}
});
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.enable('trust proxy');
app.set('json spaces', 20);

var models = {};
models.complaints = require("./models/complaints")(app);
models.noncomplaints = require("./models/noncomplaints")(app);
models.twitter = require("./models/twitter")(app);
app.models = models;

var controllers = {};
controllers.graph = require("./controllers/graph")(app);
controllers.data = require("./controllers/data")(app);
app.controllers = controllers;

var homeAPIroute = require("./routes/home")(express,controllers);
var v1APIroute = require("./routes/v1")(express, controllers);

app.use('/docs', express.static('docs'));
app.use('/', homeAPIroute);
app.use('/v1', v1APIroute);


var server = app.listen(app.get('port'), 'localhost', function(){
  console.log("Express server listening on port " + app.get('port'));
});
