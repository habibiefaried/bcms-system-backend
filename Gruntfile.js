"use strict";

module.exports = function(grunt) {
	grunt.initConfig({
		jshint: {
		      files: ['Gruntfile.js', 'controllers/*.js', 'models/*.js','routes/*.js','app,js'],
		      options: {
		        globalstrict: true,
				globals: {
					global: true,
					helper: true,
					__dirname: true,
					process: true,
					jQuery: true,
					console: true,
					module: true,
					logger: true,
					require: true,
					Buffer: true,
					_: true
				}
		      }
		    },
		apidoc: {
		  bcms: {
		    src: "routes/",
		    dest: "docs/"
		  }
		}
	});
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-apidoc');
};