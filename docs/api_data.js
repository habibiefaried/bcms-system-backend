define({ "api": [
  {
    "type": "get",
    "url": "/v1/data/complaints?tgl_awal=2015-10-06&tgl_akhir=2015-10-11",
    "title": "Get Complaints",
    "name": "getComplaint",
    "description": "<p>Mengambil seluruh data yang bersifat complaint. Terdapat 2 parameter yaitu tgl_akhir dan tgl_awal yang bersifat opsional.</p> ",
    "group": "Data",
    "version": "1.1.0",
    "filename": "routes/v1.js",
    "groupTitle": "Data",
    "sampleRequest": [
      {
        "url": "http://bcms-api.habibiefaried.com/v1/data/complaints?tgl_awal=2015-10-06&tgl_akhir=2015-10-11"
      }
    ]
  },
  {
    "type": "get",
    "url": "/v1/data/noncomplaints",
    "title": "Get Non Complaints",
    "name": "getNonComplaint",
    "description": "<p>Mengambil seluruh data yang bukan bersifat complaint</p> ",
    "group": "Data",
    "version": "1.1.0",
    "filename": "routes/v1.js",
    "groupTitle": "Data",
    "sampleRequest": [
      {
        "url": "http://bcms-api.habibiefaried.com/v1/data/noncomplaints"
      }
    ]
  },
  {
    "type": "post",
    "url": "/v1/data/complaint/insert",
    "title": "Insert Complaint",
    "name": "insertComplaint",
    "description": "<p>Memasukkan data komplain baru ke BCMS API</p> ",
    "group": "Data",
    "version": "1.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "text",
            "description": "<p>Kalimat komplain</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>Object</p> ",
            "optional": false,
            "field": "destinations",
            "description": "<p>Tujuan komplain, dapat berupa array atau string</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "source",
            "description": "<p>Asal komplain (TWITTER, FACEBOOK, LAPOR)</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "topic",
            "description": "<p>Topik komplain</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "status",
            "description": "<p>Status komplain (NEW, PENDING, OPEN, dkk)</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Parameter-Example:",
          "content": " { \n \t\"text\" : \"Pak, saya mau komplain\",\n  \t\"destinations\" : \"Ridwan Kamil\",\n  \t\"source\" : \"LAPOR\",\n  \t\"topic\" : \"Umum\",\n     \"status\" : \"NEW\",\n\t}",
          "type": "json"
        }
      ]
    },
    "filename": "routes/v1.js",
    "groupTitle": "Data",
    "sampleRequest": [
      {
        "url": "http://bcms-api.habibiefaried.com/v1/data/complaint/insert"
      }
    ]
  },
  {
    "type": "post",
    "url": "/v1/data/twitter/insert",
    "title": "Insert Twitter Data",
    "name": "insertTwitterData",
    "description": "<p>Memasukkan data twitter ke BCMS API</p> ",
    "group": "Data",
    "version": "1.1.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "created_at",
            "description": "<p>Tanggal tweet digenerate</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "in_reply_to_status_id",
            "description": "<p>Reply ke status ID</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "in_reply_to_user_id",
            "description": "<p>Reply ke user ID</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "source",
            "description": "<p>Source</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "retweet_count",
            "description": "<p>jumlah di retweet</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "retweeted",
            "description": "<p>apakah di retweet?</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "filter_level",
            "description": "<p>Level Filter</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "is_quote_status",
            "description": "<p>Apakah status berquote</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "tweet_id",
            "description": "<p>ID Tweet</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "text",
            "description": "<p>Teks tweet</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>Number</p> ",
            "optional": false,
            "field": "favorite_count",
            "description": "<p>Jumlah yang favorite</p> "
          },
          {
            "group": "Parameter",
            "type": "<p>String</p> ",
            "optional": false,
            "field": "location",
            "description": "<p>Lokasi</p> "
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": " { \n \t\"created_at\" : \"Mon Oct 05 05:20:39 +0000 2015\",\n  \t\"in_reply_to_status_id\" : \"650867856872112128\",\n  \t\"in_reply_to_user_id\" : \"2367011946\",\n  \t\"source\" : \"<a href=\\\"http:\\/\\/twitter.com\\/download\\/android\\\" rel=\\\"nofollow\\\">Twitter for Android<\\/a>\",\n  \t\"retweet_count\" : 1,\n  \t\"retweeted\" : \"true\",\n  \t\"filter_level\" : \"low\",\n  \t\"is_quote_status\" : \"false\",\n  \t\"tweet_id\" : \"650903403472646144\",\n  \t\"text\" : \"@kikimaliki81 @Viking_Buas @yanabool @ridwankamil Haloo semuanya\",\n  \t\"favorite_count\" : 2,\n  \t\"location\" : \"Bandung\",\n\t}",
          "type": "json"
        }
      ]
    },
    "filename": "routes/v1.js",
    "groupTitle": "Data",
    "sampleRequest": [
      {
        "url": "http://bcms-api.habibiefaried.com/v1/data/twitter/insert"
      }
    ]
  },
  {
    "type": "get",
    "url": "/v1/graph/getAllTopics",
    "title": "Get All Topics",
    "name": "getAllTopics",
    "description": "<p>Mengambil jumlah komplain berdasarkan topik komplain</p> ",
    "group": "Graph",
    "version": "1.1.0",
    "filename": "routes/v1.js",
    "groupTitle": "Graph",
    "sampleRequest": [
      {
        "url": "http://bcms-api.habibiefaried.com/v1/graph/getAllTopics"
      }
    ]
  },
  {
    "type": "get",
    "url": "/v1/graph/getDataStream",
    "title": "Get Data Stream",
    "name": "getDataStream",
    "description": "<p>Mengambil jumlah komplain dari waktu ke waktu</p> ",
    "group": "Graph",
    "version": "1.1.0",
    "filename": "routes/v1.js",
    "groupTitle": "Graph",
    "sampleRequest": [
      {
        "url": "http://bcms-api.habibiefaried.com/v1/graph/getDataStream"
      }
    ]
  },
  {
    "type": "get",
    "url": "/v1/graph/getJmlMasalah",
    "title": "Get Jumlah Masalah",
    "name": "getJmlMasalah",
    "description": "<p>Mengambil jumlah komplain seluruhnya</p> ",
    "group": "Graph",
    "version": "1.1.0",
    "filename": "routes/v1.js",
    "groupTitle": "Graph",
    "sampleRequest": [
      {
        "url": "http://bcms-api.habibiefaried.com/v1/graph/getJmlMasalah"
      }
    ]
  },
  {
    "type": "get",
    "url": "/v1/graph/getSource",
    "title": "Get Jumlah Source",
    "name": "getSource",
    "description": "<p>Mengambil jumlah komplain berdasarkan asal komplain tersebut</p> ",
    "group": "Graph",
    "version": "1.1.0",
    "filename": "routes/v1.js",
    "groupTitle": "Graph",
    "sampleRequest": [
      {
        "url": "http://bcms-api.habibiefaried.com/v1/graph/getSource"
      }
    ]
  },
  {
    "type": "get",
    "url": "/v1",
    "title": "Get Index",
    "name": "getIndex",
    "description": "<p>getIndex</p> ",
    "group": "Index",
    "version": "1.1.0",
    "filename": "routes/v1.js",
    "groupTitle": "Index",
    "sampleRequest": [
      {
        "url": "http://bcms-api.habibiefaried.com/v1"
      }
    ]
  },
  {
    "type": "get",
    "url": "/v1/data/twitter",
    "title": "Get All Tweet",
    "name": "getTwitter",
    "description": "<p>Mengambil seluruh data twitter</p> ",
    "group": "Twitter",
    "version": "1.1.0",
    "filename": "routes/v1.js",
    "groupTitle": "Twitter",
    "sampleRequest": [
      {
        "url": "http://bcms-api.habibiefaried.com/v1/data/twitter"
      }
    ]
  },
  {
    "type": "get",
    "url": "/v1/data/twitter/650903403472646144",
    "title": "Get Tweet ID",
    "name": "getTwitterID",
    "description": "<p>Mengambil data twitter dengan id tertentu. Contoh ID twitter adalah diatas (650903403472646144)</p> ",
    "group": "Twitter",
    "version": "1.1.0",
    "filename": "routes/v1.js",
    "groupTitle": "Twitter",
    "sampleRequest": [
      {
        "url": "http://bcms-api.habibiefaried.com/v1/data/twitter/650903403472646144"
      }
    ]
  }
] });