define({
  "name": "BCMS API Documentation",
  "version": "0.1.0",
  "description": "API untuk sistem BCMS dengan tujuan open data",
  "title": "BCMS API Documentation",
  "url": "http://bcms-api.habibiefaried.com",
  "sampleUrl": "http://bcms-api.habibiefaried.com",
  "apidoc": "0.2.0",
  "generator": {
    "name": "apidoc",
    "time": "2016-01-28T00:14:39.440Z",
    "url": "http://apidocjs.com",
    "version": "0.13.1"
  }
});