(function(){
    "use strict";

module.exports = function(app){
	var controller = {};
	var model = app.models;
	
	controller.complaintData = function(tgl_awal, tgl_akhir, callback){
		model.complaints.findAll(tgl_awal, tgl_akhir, function(err, data){
			if (err) callback(err);
			else callback(null, data);
		});
	};

	controller.twitterData = function(callback){
		model.twitter.findAll(function(err, data){
			if (err) callback(err);
			else callback(null, data);
		});
	};

	controller.twitterDataID = function(tweet_id, callback){
		model.twitter.findByTweetId(tweet_id, function(err, data){
			if (err) callback(err);
			else callback(null, data);
		});
	};

	controller.nonComplaintData = function(callback){
		model.noncomplaints.findAll(function(err, data){
			if (err) callback(err);
			else callback(null, data);
		});
	};

	controller.insertComplaintData = function(_c, callback){
		model.complaints.createNew(_c, function(err,data){
			if (err) callback(err);
			else callback(null, data);
		});
	};	
	
	controller.insertTwitterData = function(_c, callback){
		model.twitter.createNew(_c, function(err,data){
			if (err) callback(err);
			else callback(null, data);
		});
	};
	return controller;
};

})();