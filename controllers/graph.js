(function(){
    "use strict";

module.exports = function(app){
	var controller = {};
	var model = app.models;
	
	controller.countMasalah = function(callback){
		// should return (list of <kota,jumlah>)
		// blom bisa karena gk ada kota
		model.complaints.countAll(function(err,data){
			if (err) callback(err);
			else callback(null, data);
		});
	};

	controller.countSource = function(callback){
		model.complaints.countFilter("source",["TWITTER","FACEBOOK","LAPOR"], function(err,data){
			if (err) callback(err);
			else callback(null, data);
		});
	};

	controller.countTopic = function(callback){
		model.complaints.distinct("topic", function(err,data){
			if (err) callback(err);
			else {
				model.complaints.countFilter("topic",data, function(err,data2){
					if (err) callback(err);
					else callback(null, data2);
				});
			}
		});
	};

	controller.countGraphData = function(callback){
		model.complaints.countFilterTgl(function(err,data){
			if (err) callback(err);
			else callback(null, data);
		});
	};

	return controller;
};

})();