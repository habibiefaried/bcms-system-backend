(function(){
    "use strict";
    
module.exports = function(app){
	var collection = 'noncomplaints';
	var mongoose = app.mongoose;
	var Schema = mongoose.Schema;
	var ObjectId = Schema.ObjectId;

	var schema = new Schema({
		_id: ObjectId,
		id: String,
		text: String,
		timestamp: Number,
		date: Date,
	});

	var Model = mongoose.model(collection, schema);

	Model.findAll = function(callback){
		Model.find({}, function(err, _c){
			if (err) callback(err);
			else callback(null, _c);
		});
	};

	
	return Model;
};

})();