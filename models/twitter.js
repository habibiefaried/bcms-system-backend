(function(){
    "use strict";

/*
{
   "in_reply_to_status_id_str":"650867856872112128",
   "in_reply_to_status_id":650867856872112128,
   "created_at":"Mon Oct 05 05:20:39 +0000 2015",
   "in_reply_to_user_id_str":"2367011946",
   "source":"s",
   "retweet_count":0,
   "retweeted":false,
   "geo":null,
   "filter_level":"low",
   "in_reply_to_screen_name":"kikimaliki81",
   "is_quote_status":false,
   "id_str":"650903403472646144",
   "in_reply_to_user_id":2367011946,
   "favorite_count":0,
   "id":650903403472646144,
   "text":"@kikimaliki81 @Viking_Buas @yanabool @ridwankamil",
   "place":null,
   "lang":"und",
   "favorited":false,
   "coordinates":null,
   "truncated":false,
   "timestamp_ms":"1444022439245",
   "entities":{
      "urls":[

      ],
      "hashtags":[

      ],
      "user_mentions":[
         {
            "indices":[
               0,
               13
            ],
            "screen_name":"kikimaliki81",
            "id_str":"2367011946",
            "name":"Kiki Maliki",
            "id":2367011946
         },
         {
            "indices":[
               14,
               26
            ],
            "screen_name":"Viking_Buas",
            "id_str":"1215783746",
            "name":"Viking Bumi Asri",
            "id":1215783746
         },
         {
            "indices":[
               27,
               36
            ],
            "screen_name":"yanabool",
            "id_str":"325924335",
            "name":"komandan hangsip",
            "id":325924335
         },
         {
            "indices":[
               37,
               49
            ],
            "screen_name":"ridwankamil",
            "id_str":"80323736",
            "name":"ridwan kamil",
            "id":80323736
         }
      ],
      "symbols":[

      ]
   },
   "contributors":null,
   "user":{
      "utc_offset":25200,
      "friends_count":91,
      "profile_image_url_https":"https:\/\/pbs.twimg.com\/profile_images\/520364737880539138\/1-njqaNc_normal.jpeg",
      "listed_count":4,
      "profile_background_image_url":"http:\/\/abs.twimg.com\/images\/themes\/theme1\/bg.png",
      "default_profile_image":false,
      "favourites_count":NumberInt(311),
      "description":"Berusaha menjalani hidup sesuai guide yang ada . . .      MDPL",
      "created_at":"Thu Dec 26 16:42:12 +0000 2013",
      "is_translator":false,
      "profile_background_image_url_https":"https:\/\/abs.twimg.com\/images\/themes\/theme1\/bg.png",
      "protected":false,
      "screen_name":"Dzatmycho",
      "id_str":"2263061474",
      "profile_link_color":"0084B4",
      "id":2263061474,
      "geo_enabled":true,
      "profile_background_color":"C0DEED",
      "lang":"id",
      "profile_sidebar_border_color":"C0DEED",
      "profile_text_color":"333333",
      "verified":false,
      "profile_image_url":"http:\/\/pbs.twimg.com\/profile_images\/520364737880539138\/1-njqaNc_normal.jpeg",
      "time_zone":"Bangkok",
      "url":null,
      "contributors_enabled":false,
      "profile_background_tile":false,
      "profile_banner_url":"https:\/\/pbs.twimg.com\/profile_banners\/2263061474\/1411296528",
      "statuses_count":11281,
      "follow_request_sent":null,
      "followers_count":118,
      "profile_use_background_image":true,
      "default_profile":true,
      "following":null,
      "name":"Dzat cikiny stones​",
      "location":"Bandung ",
      "profile_sidebar_fill_color":"DDEEF6",
      "notifications":null
   }
}
*/
module.exports = function(app){
	var collection = 'twitter';
	var mongoose = app.mongoose;
	var Schema = mongoose.Schema;
	var ObjectId = Schema.ObjectId;

	var schema = new Schema({
		_id: ObjectId,
		created_at: Date,
		in_reply_to_status_id: String,
		in_reply_to_user_id: String,
		source: String,
		retweet_count: Number,
		retweeted: Boolean,
		filter_level: String,
		is_quote_status: Boolean,
		tweet_id: String,
		text: String,
		favorite_count: Number,
		location: String,
	});

	var Model = mongoose.model(collection, schema);

	Model.findAll = function(callback){
		Model.find({}, function(err, _c){
			if (err) callback(err);
			else callback(null, _c);
		});
	};

	Model.findByTweetId = function(tid, callback){
		Model.findOne({tweet_id: tid}, function(err, _c){
			if (err) callback(err);
			else callback(null, _c);
		});
	};
	
	Model.createNew = function(_data, callback){
		_data._id = new mongoose.Types.ObjectId();
		var _d = new Model(_data);

		_d.save(function(err,obj){
			if (err) callback(err);
			else callback(null, obj.toObject());
		});
	};

	return Model;
};

})();