(function(){
    "use strict";

module.exports = function(app){
	var collection = 'complaints';
	var mongoose = app.mongoose;
	var Schema = mongoose.Schema;
	var ObjectId = Schema.ObjectId;

	var schema = new Schema({
		_id: ObjectId,
		id: String,
		text: String,
		timestamp: Number,
		date: Date,
		destinations: Object,
		topic: String,
		location: {
			name: String,
			lattitude: String,
			longitude: String,
		},
		source: String,
		status: String,
	});

	var Model = mongoose.model(collection, schema);

	Model.findAll = function(tgl_awal, tgl_akhir, callback){
		if ((typeof tgl_awal=="undefined") || (typeof tgl_akhir=="undefined")) {
			Model.find({}, '_id topic source text date destinations location', {
				    sort:{
				        date: -1 //Sort by Date Added DESC
				    }
				}, function(err, _c){
						if (err) callback(err);
						else {
							var result = {};
							result.data = _c;
							result.pesan = "Semua Data";
							result.count = _c.length;
							callback(null, result);
						}
			});
		} else {
			var tgla = new Date(tgl_awal);
			var tglb = new Date(tgl_akhir);
			Model.find({date: {$gt: tgla, $lt: tglb}}, '_id topic source text status date destinations location', {
				    sort:{
				        date: -1 //Sort by Date Added DESC
				    }
				}, function(err, _c) {
						if (err) callback(err);
						else {
							var result = {};
							result.data = _c;
							result.pesan = "Data antara tanggal "+tgl_awal+" sampai "+tgl_akhir;
							result.count = _c.length;
							callback(null, result);
						}
			});
		}
	};

	Model.countAll = function(callback){
		Model.count({}, function(err, result){
			if (err) callback(err);
			else callback(null, result);
		});
	};

	/*
	countFilter digunakan untuk melakukan perhitungan terhadap field yang spesifik
	contohnya adalah penghitungan source [FACEBOOK, TWITTER, LAPOR]. String Matching
	*/
	Model.countFilter = function(dest_field, arr_string, callback){
		var result = {};
		for (var i=0; i<arr_string.length; i++){
			result[arr_string[i]] = 0;
		}
		Model.find({}, function(err, data){
			if (err) callback(err);
			else {
				for (var j=0; j<data.length; j++){
					for (var k=0; k<arr_string.length; k++){
						if (data[j][dest_field] === arr_string[k]) {
							result[arr_string[k]]++;
							break;
						}
					}
				}
			}
			callback(null, result);
		});
	};

	/** Select distinct terhadap field yang diberikan 
	*/
	Model.selectDistinct = function(field, callback){
		Model.find().distinct(field, function(err, data) {
		    if (err) callback(err);
		    else callback(null, data);
		});
	};

	function toStringDate(d){
		var tanggal = d.getDate();
		var bulan = d.getMonth();
		var tahun = d.getFullYear();
		return tanggal+"-"+bulan+"-"+tahun;
	}

	/* countFilterTgl
	Khusus digunakan untuk perhitungan banyaknya twit pada hari tertentu
	contoh ["2014-09-20","2014-09-21","2014-09-22"]. dst..
	*/

	Model.countFilterTgl = function(callback){
		Model.find({}, {}, {sort: {date: 1}}, function(err,data){
			if (err) callback(err);
			else {
				var hasil = {};
				for (var i=0; i<data.length; i++){
					if (!hasil.hasOwnProperty(toStringDate(data[i].date)))  //kalau gk ada, 0
						hasil[toStringDate(data[i].date)] = 0;
					else
						hasil[toStringDate(data[i].date)]++;
				}
				callback(null, hasil);
			}
		});
	};	

	Model.createNew = function(_data, callback){
		_data._id = new mongoose.Types.ObjectId();
		var _d = new Model(_data);

		_d.save(function(err,obj){
			if (err) callback(err);
			else callback(null, obj.toObject());
		});
	};

	return Model;
};

})();