(function(){
    "use strict";

module.exports = function(express, controllers){
	var router = express.Router();

	/**
	 * Author: habibiefaried
	 *
	 * @api {get} /v1 Get Index
	 * @apiName getIndex
	 * @apiDescription getIndex
	 * @apiGroup Index
	 * @apiVersion 1.1.0
	 *
	 * 
	 *
	 *
	 **/
	router.get('/', function(req, res) {
		res.json({ message: 'Welcome to BCMS API (version 1)' });   
	});

	/**
	 * Author: habibiefaried
	 *
	 * @api {get} /v1/data/complaints?tgl_awal=2015-10-06&tgl_akhir=2015-10-11 Get Complaints
	 * @apiName getComplaint
	 * @apiDescription Mengambil seluruh data yang bersifat complaint. Terdapat 2 parameter yaitu tgl_akhir dan tgl_awal yang bersifat opsional.
	 * @apiGroup Data
	 * @apiVersion 1.1.0
	 *
	 * 
	 **/
	router.get('/data/complaints', function(req, res) {
		controllers.data.complaintData(req.sanitize(req.query.tgl_awal), req.sanitize(req.query.tgl_akhir), function(err, data){
			if (err) res.status(500).send(err);
			else res.status(200).json(data);
		});   
	});

	/**
	 * Author: habibiefaried
	 *
	 * @api {get} /v1/data/noncomplaints Get Non Complaints
	 * @apiName getNonComplaint
	 * @apiDescription Mengambil seluruh data yang bukan bersifat complaint
	 * @apiGroup Data
	 * @apiVersion 1.1.0
	 *
	 * 
	 *
	 *
	 **/
	router.get('/data/noncomplaints', function(req,res){
		controllers.data.nonComplaintData(function(err, data){
			if (err) res.status(500).send(err);
			else res.status(200).json(data);
		});
	});

	/**
	 * Author: habibiefaried
	 *
	 * @api {get} /v1/data/twitter Get All Tweet
	 * @apiName getTwitter
	 * @apiDescription Mengambil seluruh data twitter
	 * @apiGroup Twitter
	 * @apiVersion 1.1.0
	 *
	 * 
	 *
	 *
	 **/
	router.get('/data/twitter', function(req, res) {
		controllers.data.twitterData(function(err, data){
			if (err) res.status(500).send(err);
			else res.status(200).json(data);
		});   
	});

	/**
	 * Author: habibiefaried
	 *
	 * @api {get} /v1/data/twitter/650903403472646144 Get Tweet ID
	 * @apiName getTwitterID
	 * @apiDescription Mengambil data twitter dengan id tertentu. Contoh ID twitter adalah diatas (650903403472646144)
	 * @apiGroup Twitter
	 * @apiVersion 1.1.0
	 *
	 * 
	 *
	 *
	 **/
	router.get('/data/twitter/:id', function(req, res) {
		controllers.data.twitterDataID(req.params.id, function(err, data){
			if (err) res.status(500).send(err);
			else res.status(200).json(data);
		});   
	});

	/**
	 * Author: habibiefaried
	 *
	 * @api {get} /v1/graph/getJmlMasalah Get Jumlah Masalah
	 * @apiName getJmlMasalah
	 * @apiDescription Mengambil jumlah komplain seluruhnya
	 * @apiGroup Graph
	 * @apiVersion 1.1.0
	 *
	 * 
	 *
	 *
	 **/
	router.get('/graph/getJmlMasalah', function(req,res){
		controllers.graph.countMasalah(function(err,data){
			if (err) res.status(500).send(err);
			else res.status(200).json(data);
		});
	});


	/**
	 * Author: habibiefaried
	 *
	 * @api {get} /v1/graph/getSource Get Jumlah Source
	 * @apiName getSource
	 * @apiDescription Mengambil jumlah komplain berdasarkan asal komplain tersebut
	 * @apiGroup Graph
	 * @apiVersion 1.1.0
	 *
	 * 
	 *
	 *
	 **/
	router.get('/graph/getSource', function(req,res){
		controllers.graph.countSource(function(err,data){
			if (err) res.status(500).send(err);
			else res.status(200).json(data);
		});
	});


	/**
	 * Author: habibiefaried
	 *
	 * @api {get} /v1/graph/getAllTopics Get All Topics
	 * @apiName getAllTopics
	 * @apiDescription Mengambil jumlah komplain berdasarkan topik komplain
	 * @apiGroup Graph
	 * @apiVersion 1.1.0
	 *
	 * 
	 *
	 *
	 **/
	router.get('/graph/getAllTopics', function(req,res){
		controllers.graph.countTopic(function(err,data){
			if (err) res.status(500).send(err);
			else res.status(200).json(data);
		});
	});

	/**
	 * Author: habibiefaried
	 *
	 * @api {get} /v1/graph/getDataStream Get Data Stream
	 * @apiName getDataStream
	 * @apiDescription Mengambil jumlah komplain dari waktu ke waktu
	 * @apiGroup Graph
	 * @apiVersion 1.1.0
	 *
	 * 
	 *
	 *
	 **/
	router.get('/graph/getDataStream', function(req,res){
		controllers.graph.countGraphData(function(err,data){
			if (err) res.status(500).send(err);
			else res.status(200).json(data);
		});
	});

	/**
	 * Author: habibiefaried
	 *
	 * @api {post} /v1/data/complaint/insert Insert Complaint
	 * @apiName insertComplaint
	 * @apiDescription Memasukkan data komplain baru ke BCMS API
	 * @apiGroup Data
	 * @apiVersion 1.1.0
	 *
	 * @apiParam {String} text Kalimat komplain
	 * @apiParam {Object} destinations Tujuan komplain, dapat berupa array atau string
	 * @apiParam {String} source Asal komplain (TWITTER, FACEBOOK, LAPOR)
	 * @apiParam {String} topic Topik komplain
	 * @apiParam {String} status Status komplain (NEW, PENDING, OPEN, dkk)
	 * @apiParamExample {json} Parameter-Example:
	 *  { 
	 *  	"text" : "Pak, saya mau komplain",
	 *   	"destinations" : "Ridwan Kamil",
	 *   	"source" : "LAPOR",
	 *   	"topic" : "Umum",
	 *      "status" : "NEW",
	 *	}
	 *
	 **/
	router.post('/data/complaint/insert', function(req,res){
		var new_data = {};
		new_data.text = req.sanitize(req.body.text);
		new_data.date = new Date();
		new_data.destinations = req.sanitize(req.body.destinations);
		new_data.topic = req.sanitize(req.body.topic);
		new_data.source = req.sanitize(req.body.source);
		new_data.status = req.sanitize(req.body.status);
		controllers.data.insertComplaintData(new_data,function(err,data){
			if (err) res.status(500).send(err);
			else res.status(200).json(data);
		});
	});

	/**
	 * Author: habibiefaried
	 *
	 * @api {post} /v1/data/twitter/insert Insert Twitter Data
	 * @apiName insertTwitterData
	 * @apiDescription Memasukkan data twitter ke BCMS API
	 * @apiGroup Data
	 * @apiVersion 1.1.0
	 *
	 * @apiParam {String} created_at Tanggal tweet digenerate
	 * @apiParam {String} in_reply_to_status_id Reply ke status ID
	 * @apiParam {String} in_reply_to_user_id Reply ke user ID
	 * @apiParam {String} source Source
	 * @apiParam {Number} retweet_count jumlah di retweet
	 * @apiParam {String} retweeted apakah di retweet?
	 * @apiParam {String} filter_level Level Filter
	 * @apiParam {String} is_quote_status Apakah status berquote
	 * @apiParam {String} tweet_id ID Tweet
	 * @apiParam {String} text Teks tweet
	 * @apiParam {Number} favorite_count Jumlah yang favorite
	 * @apiParam {String} location Lokasi
	 * @apiParamExample {json} Request-Example:
	 *  { 
	 *  	"created_at" : "Mon Oct 05 05:20:39 +0000 2015",
	 *   	"in_reply_to_status_id" : "650867856872112128",
	 *   	"in_reply_to_user_id" : "2367011946",
	 *   	"source" : "<a href=\"http:\/\/twitter.com\/download\/android\" rel=\"nofollow\">Twitter for Android<\/a>",
	 *   	"retweet_count" : 1,
	 *   	"retweeted" : "true",
	 *   	"filter_level" : "low",
	 *   	"is_quote_status" : "false",
	 *   	"tweet_id" : "650903403472646144",
	 *   	"text" : "@kikimaliki81 @Viking_Buas @yanabool @ridwankamil Haloo semuanya",
	 *   	"favorite_count" : 2,
	 *   	"location" : "Bandung",
	 *	}
	 *
	 **/
	router.post('/data/twitter/insert', function(req,res){
		var new_data = {};
		new_data.created_at = new Date(req.body.created_at);
		new_data.in_reply_to_status_id = req.sanitize(req.body.in_reply_to_status_id);
		new_data.in_reply_to_user_id = req.sanitize(req.body.in_reply_to_user_id);
		new_data.source = req.sanitize(req.body.source);
		new_data.retweet_count = req.sanitize(req.body.retweet_count);
		new_data.retweeted = req.body.retweeted.toLowerCase() == "true";
		new_data.filter_level = req.sanitize(req.body.filter_level);
		new_data.is_quote_status = req.body.is_quote_status.toLowerCase() == "true";
		new_data.tweet_id = req.sanitize(req.body.tweet_id);
		new_data.text = req.sanitize(req.body.text);
		new_data.favorite_count = req.sanitize(req.body.favorite_count);
		new_data.location = req.sanitize(req.body.location);
		controllers.data.insertTwitterData(new_data, function(err,data){
			if (err) res.status(500).send(err);
			else res.status(200).json(data);
		});
	});

	return router;
};

})();