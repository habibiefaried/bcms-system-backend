(function(){
    "use strict";

module.exports = function(express, controllers){
	var router = express.Router();

	router.get('/', function(req, res) {
		res.json({ message: 'Welcome to BCMS API!' });   
	});
	return router;
};

})();